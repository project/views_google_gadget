
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Advice

INTRODUCTION
------------

Current Maintainer: Bastlynn <bastlynn@gmail.com>

The Views Google gadget module allows you to create views to be displayed in a Google gadget. These gadgets 
allow you to expose your site information to the iGoogle homepage as well as other Google locations. 

Note: This implementation is of type url. The views are url content referenced by a XML definition. 
See: http://code.google.com/apis/gadgets/docs/fundamentals.html#Content_Type

Why? Because we need to be able to login to control the user access to the view. HTML content types are
not user aware. Available means of logging a user in from a Google gadget are insufficient for general use.
See: http://code.google.com/apis/gadgets/docs/fundamentals.html#Cookies

For more information:

http://www.google.com/webmasters/gadgets/
http://code.google.com/apis/gadgets/
http://code.google.com/apis/igoogle/docs/anatomy.html

INSTALLATION
------------

1. Copy this views_google_gadget/ directory to your sites/SITENAME/modules directory. 

2. Enable the module.

3. After you create your view, make the path/gadget.xml URL available to your users or the Google directory.

ADVICE
------

1. Testing

You want to test your gadget before releasing it to the world at large or trying to get it listed with Google.
If you want to test it out on your iGoogle page, first install the developer gadget:

See: http://code.google.com/apis/igoogle/docs/igoogledevguide.html#dev_env

Then you can plug the XML URL in and see the view on your homepage.

2. Links and IFrames

Google gadgets display within an iframe. This means any links you click on from the gadget, stay within the frame.
Depending on your needs, this may be good behavior for your gadget - or it may be horrible. Either way, please keep
it in mind. This module does not made any changes to how links are displayed or interacted with. This is deliberate.
I can't predict what you are going to want to do with your gadget. There are a few ways to make sure your links go 
where you need them to, and I encourage you to use them where you need to.

First: You can use target="_top" in your links, that will break the link from any framing element

Second: You could use javascript to break users out if they click on a link on the site. Attach the script to the
onclick event for the link. Don't auto run the script on page load - that defeats the purpose of a gadget. Make use
of jquery selectors to get to all the links you need to easily.

See: http://api.jquery.com/click/

<script language="JavaScript" type="text/javascript">
<!--
$('#target').click(function() {
  if (top.location.href != this.attr('href')) {
    top.location.href = this.attr('href') ;
  }
});
-->
</script>