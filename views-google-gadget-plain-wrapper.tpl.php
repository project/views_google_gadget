<?php

/**
 * @file
 * Produces a plain output of the view.
 *
 * Incoming variables:  $args
 */

$content = $args['content'];
$login_form = '';
if (!empty($args['login_form'])) {
  $login_form = ''.
  '<div id="login_form" class="login_form">'."\n".
  $args['login_form'] ."\n".
  '</div>'."\n";
}

print ''.
'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'."\n".
'<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">'."\n".
'  <head>'."\n".
'    '. $head ."\n".
'    <title>'. drupal_get_title() .'</title>'."\n".
'    '. $styles ."\n".
'    '. $scripts ."\n".
'  </head>'."\n".
'<body>'."\n".
$content ."\n".
$login_form ."\n".
'</body>'."\n".
'</html>'.
'';