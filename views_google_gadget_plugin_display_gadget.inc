<?php

/**
 * @file
 * Contains the Google gadget display plugin.
 */

class views_google_gadget_plugin_display_gadget extends views_plugin_display_page {

 /**
  * Override the page settings, we don't need or want a breadcrumb here.
  */
  function uses_breadcrumb() {
    return FALSE;
  }

 /**
  * Called when view is displayed or gadget is displayed.
  */
  function execute() {
    // Look for gadget path arguement.
    $args = array();
    $gadget_path = $this->get_option('gadget_path');
    $view_args = $this->view->args;

    // All of this is to figure out what our real view path is.
    // This is so we can tell the xml and login forms where the gadget view is.
    $bits = explode('/', $this->get_option('path'));
    foreach ($bits as $pos => $bit) {
      $bits[$pos] = $bit;
      if ($bit == '%') {
        $bits[$pos] = current($view_args);
        next($view_args);
      }
    }
    $view_path = implode('/', $bits);

    // Check if we're in the XML path or the view path.
    if (in_array($gadget_path, $view_args)) {
      $gadget_path = array_pop($view_args);
      $options = array();
      $option_names = _views_google_gadget_options_list();
      foreach ($option_names as $name => $description) {
        $options[$name] = $this->get_option($name);
      }
      $args['options'] = $options;
      $args['path'] = url($view_path, array('absolute' => TRUE));
      $output = theme('views_google_gadget_gadget', $args);
      if (empty($output)) {
        return drupal_not_found();
      }
    }
    else {
      // Check if we need to prepare a login form for users otherwise denied access.
      $args['login_form'] = '';
      global $user;

      // Provide a login for any anon. user (even if they're otherwise allowed to see the content)
      $show_login = $this->get_option('show_login');
      if (($show_login == 1) && ($user->uid == 0)) {
        // Yes, we need to create a login.
        // But we want to redirect back to the view, so can't call it directly.
        $login_form = drupal_get_form('_views_google_gadget_user_login', $view_path);
        $args['login_form'] = $login_form;
      }

      // Check for legitimate access at this point to this URL.
      // This is code normally called during menu creation (which we overrode).
      // So the same security is applied - just at a different spot in the display process.
      $access_plugin = $this->get_access_plugin();
      if (!isset($access_plugin)) {
        $access_plugin = views_get_plugin('access', 'none');
      }
      $allowed = views_access($access_plugin->get_access_callback());

      if (!$allowed) {
        $args['content'] = t('Please login to use this gadget.');
        $output = theme('views_google_gadget_plain_wrapper', $args);
      }
      else {
        // This is where framing to the theme may be injected: we run results through theme().
        // All we need to do is make sure they're in plain wrapper of html for clarity.
        views_set_page_view($this);

        // Prior to this being called, the $view should already be set to this
        // display, and arguments should be set on the view.
        $this->view->build();
        if (!empty($this->view->build_info['fail'])) {
          return drupal_not_found();
        }

        drupal_set_title(filter_xss_admin($this->view->get_title()));
        $output = $this->view->render();
        if (empty($output)) {
          return drupal_not_found();
        }
        $args['content'] = $output;
        $output = theme('views_google_gadget_plain_wrapper', $args);
      }
    }
    // This is the magic spot.
    // Here is where the page would normally return back to the page theme for display.
    // Instead we don't return anything, and print instead.
    // That way we don't wrap the visually heavy theme around the gadget, since we're using the plain wrapper.
    // So when it displays on a remote page (iGoogle), it looks right.
    print $output;
  }

  /**
   * Add the view path and gadget information to Drupal's menu system.
   */
  function execute_hook_menu() {
    $items = array();
    // The gadget we need to make sure is made in addition to the existing view.
    // Google gadgets (of the URL flavour) are composed of two parts:
    // One the actual output - here, the view.
    // The second is the XML definition of the gadget - which is what is included on say iGoogle's homepage.
    $items = $this->_execute_google_gadget_menu('gadget_path', $items);
    $items = $this->_execute_google_gadget_menu('path', $items);
    return $items;
  }

  /**
   * This is a replacement for the standard page menu for the view.
   * This is provided to get around the menu_access rule that would prevent
   * user login from being able to be displayed to anon. users.
   * Instead the login is noted as forbidden but the response is not to block,
   * but instead to change the output to a login form. (Why is this not a standard option for menu?)
   */
  function _execute_google_gadget_menu($this_path, $items) {

    $bits = explode('/', $this->get_option($this_path));
    // Snag the path to figure out the args leading here.
    $page_arguments = array($this->view->name, $this->display->id);

    // Replace % with %views_arg for menu autoloading and add to the
    // page arguments so the argument actually comes through.
    foreach ($bits as $pos => $bit) {
      if ($bit == '%') {
        $bits[$pos] = '%views_arg';
        $page_arguments[] = $pos;
      }
    }

    $path = implode('/', $bits);

    // This will be called on display of the view instead.
    //$access_plugin = $this->get_access_plugin();
    //if (!isset($access_plugin)) {
    //  $access_plugin = views_get_plugin('access', 'none');
    //}

    if ($path) {
      if ($this_path == 'gadget_path') {
        $page_arguments[] = $this->get_option('gadget_path');
      }
      // Include the gadget path to pick up on the other end. So we know this is the xml not the view.
      $items[$path] = array(
        'page callback' => 'views_page',
        'page arguments' => $page_arguments,
        'access callback' => 'views_google_gadget_access', // Assures us of open access to the gadget definition.
        'access arguments' => array(),
        'load arguments'  => array($this->view->name, $this->display->id, '%index'),
      );
      $items[$path]['type'] = MENU_CALLBACK;
    }
    return $items;
  }

 /**
  * Define all the custom values that need to be saved for this view.
  */
  function option_definition() {
    $options = parent::option_definition();
    unset($options['menu']);
    // Pull our list from the module. Save on typing.
    $option_names = _views_google_gadget_options_list();
    foreach ($option_names as $name => $description) {
      $options[$name] = array('default' => '');
    }
    $options['gadget_path'] = array('default' => '');
    $options['show_login'] = array('default' => 1);
    return $options;
  }

 /**
  * Rename the page settings options, and add a new page of options for the gadget itself.
  */
  function options_summary(&$categories, &$options) {
    parent::options_summary($categories, $options);
    // The theme is going to make any menu link look wretched compared to the main site.
    // So we're not going to use a menu.
    unset($options['menu']);
    $categories['page'] = array(
      'title' => t('Gadget settings'),
    );
    $options['gadget'] = array(
      'category' => 'page',
      'title' => t('Google gadget'),
      'value' => t('Change settings'),
    );
  }

 /**
  * Modify the path wording to explain what we're doing. And add forms for the new options.
  */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    switch ($form_state['section']) {
      case 'path':
        $form['#title'] = t('Google gadget path');
        $form['path']['#title'] = t('The menu path or URL of this view');
        $form['path']['#description'] = t('This view will be displayed by visiting this path on your site. '.
        'You may use "%" in your URL to represent values that will be used for arguments: For example, "node/%/feed". '.
        'Your gadget xml definition will be found at path/gadget.xml');
        $form['gadget_path'] = array(
          '#type' => 'item',
          '#title' => t('The URL of the Google gadget XML'),
          '#description' => t('Your gadget xml definition. This is the URL that should be advertised to Google. '.
          'If you are using arguements, be sure to set the arguments to defaults before sending the gadget to Google for inclusion in '.
          'the Google directory.'),
          '#value' => url(NULL, array('absolute' => TRUE)) . (variable_get('clean_url', 0) ? '' : '?q=') . $this->get_option('gadget_path'),
        );
        break;
      case 'gadget':
        $form['#title'] = t('Google gadget settings');
        $option_names = _views_google_gadget_options_list();
        foreach ($option_names as $name => $description) {
          $current_value = $this->get_option($name);
          $form[$name] = array(
            '#title' => t($description[0]),
            '#description' => t($description[1]),
            '#type' => 'textfield',
            '#default_value' => $current_value,
          );
        }
        $form['show_login'] = array(
          '#title' => t('Show login for anonymous users'),
          '#description' => t('Provide a login block when a user is not logged in, instead of or in addition to the view.'),
          '#type' => 'checkbox',
          '#default_value' => $this->get_option('show_login'),
        );
        break;
    }
  }

 /**
  * Save options.
  */
  function options_submit(&$form, &$form_state) {
    parent::options_submit($form, $form_state);
    switch ($form_state['section']) {
      case 'path':
        $this->set_option('gadget_path', $form_state['values']['path'] .'/gadget.xml');
        break;
      case 'gadget':
        $option_names = _views_google_gadget_options_list();
        foreach ($option_names as $name => $description) {
          $this->set_option($name, $form_state['values'][$name]);
        }
        $this->set_option('show_login', $form_state['values']['show_login']);
        break;
    }
  }
}