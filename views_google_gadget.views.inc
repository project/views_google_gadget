<?php
/**
 * @file
 *  Provides the views plugin information.
 */

/**
 * Implementation of hook_views_plugin().
 */
function views_google_gadget_views_plugins() {
  return array(
    'module' => 'views_google_gadget',
    'display' => array(
      'views_google_gadget' => array(
        'title' => t('Google gadget'),
        'help' => t('Create a Google gadget.'),
        'handler' => 'views_google_gadget_plugin_display_gadget',
        'theme' => 'views_view',
        'parent' => 'page',
        'uses hook menu' => TRUE,
        'use ajax' => TRUE,
        'use pager' => TRUE,
        'accept attachments' => TRUE,
      ),
    ),
  );
}