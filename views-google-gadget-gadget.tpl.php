<?php

/**
 * @file
 * Implements the Google gadget theme.
 *
 * Incoming variables:  $args
 *
 * If you need to create a gadget that sets UserPrefs or other features.
 * Copy this one, and modify for your specific needs.
 */
header('Content-Type: text/xml');

$option_names = _views_google_gadget_options_list();
foreach ($option_names as $name => $description) {
  ${$name} = trim($args['options'][$name]);
}

$gadget_path = $args['path'];

print ''.
'<?xml version="1.0" encoding="UTF-8" ?>'."\n".
'<Module>'."\n".
'  <ModulePrefs title="'. $gadget_title .'"'."\n";
if (!empty($gadget_title_url)) { print '               title_url="'. $gadget_title_url .'"'."\n";}
if (!empty($gadget_description)) { print '               description="'. $gadget_description .'"'."\n";}
if (!empty($gadget_author)) { print '               author="'. $gadget_author .'"'."\n";}
if (!empty($gadget_author_email)) { print '               author_email="'. $gadget_author_email .'"'."\n";}
if (!empty($gadget_screenshot)) { print '               screenshot="'. $gadget_screenshot .'"'."\n";}
if (!empty($gadget_thumbnail)) { print '               thumbnail="'. $gadget_thumbnail .'"'."\n";}
if (!empty($gadget_title)) { print '               directory_title="'. $gadget_title .'"'."\n";}
if (!empty($gadget_author_affiliation)) { print '               author_affiliation="'. $gadget_author_affiliation .'"'."\n";}
if (!empty($gadget_author_location)) { print '               author_location="'. $gadget_author_location .'"'."\n";}
if (!empty($gadget_category)) { print '               category="'. $gadget_category .'"'."\n";}
if (!empty($gadget_category2)) { print '               category2="'. $gadget_category2 .'"'."\n";}
if (!empty($gadget_height)) { print '               height="'. $gadget_height .'"'."\n";}
print ''.
'    >'."\n".
'    <Require feature="dynamic-height" />'."\n".
'    <Require feature="setprefs" />'."\n".
'    <Require feature="settitle" />'."\n".
'  </ModulePrefs>'."\n".
'  <Content type="url" href="'. $gadget_path .'" />'."\n".
'</Module>'.
''; 